import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import os

## Read TLX_USERNAME and TLX_PASSWORD variables set in environment variables
tlx_username = os.environ['TLX_USERNAME']
tlx_password = os.environ['TLX_PASSWORD']


## First configure the client library to communicate with an instance of Termlex
configuration = swagger_client.Configuration()
configuration.host = 'https://uat.termlex.com'
api_client = swagger_client.ApiClient(configuration)

# create an instance of the AccountResource API, to get token
api_instance = swagger_client.AccountresourceApi()
login_dto = swagger_client.LoginDTO() # LoginDTO | loginDTO
login_dto.username = tlx_username
login_dto.password = tlx_password

# Attempt to get API key
try:
    # authorize
    api_response = api_instance.authorize_using_post(login_dto)
    key = api_response['id_token']
except ApiException as e:
    print("Exception when calling AccountresourceApi->authorize_using_post: %s\n" % e)

# Now set key as header value for all further access
print("\nUsing API Key: %s\n" %key)
api_client = swagger_client.ApiClient(header_name='Authorization', header_value='Bearer '+key)

# Getting account details
api_instance = swagger_client.AccountresourceApi(api_client)

try:
    # getAccount
    api_response = api_instance.get_account_using_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AccountresourceApi->get_account_using_get: %s\n" % e)


# Getting concept for given id
api_instance = swagger_client.ConceptresourceApi(api_client)
concept_id = 22298006 # int | id The concept id
version = 'null' # str | The version of the terminology product as date string
flavour = 'ID_ONLY' # str | The flavour of the concept - specifies details retrieved (optional)

try:
    # get the concept with given id
    api_response = api_instance.get_concept_for_id_using_get(concept_id, version, flavour=flavour)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConceptresourceApi->get_concept_for_id_using_get: %s\n" % e)