import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import os

## Read TLX_USERNAME and TLX_PASSWORD variables set in environment variables
tlx_username = os.environ['TLX_USERNAME']
tlx_password = os.environ['TLX_PASSWORD']


## First configure the client library to communicate with an instance of Termlex
configuration = swagger_client.Configuration()
configuration.host = 'https://uat.termlex.com'
api_client = swagger_client.ApiClient(configuration)

# create an instance of the AccountResource API, to get token
api_instance = swagger_client.AccountresourceApi()
login_dto = swagger_client.LoginDTO() # LoginDTO | loginDTO
login_dto.username = tlx_username
login_dto.password = tlx_password

# Attempt to get API key
try:
    # authorize
    api_response = api_instance.authorize_using_post(login_dto)
    key = api_response['id_token']
except ApiException as e:
    print("Exception when calling AccountresourceApi->authorize_using_post: %s\n" % e)

# Now set key as header value for all further access
print("\nUsing API Key: %s\n" %key)
api_client = swagger_client.ApiClient(header_name='Authorization', header_value='Bearer '+key)

def getIdDetails(idNum):
    # Getting preferred term for a concept with given id
    api_instance = swagger_client.ConceptresourceApi(api_client)
    id = idNum # str | id
    language = 'en' # str | language
    version = 20160401 # str | The version of the terminology product as date string

    try:
        # get the Preferred Term of concept with given id
         api_response = api_instance.get_concept_preferred_term_for_id_using_get(id, language, version)
         print('Preferred term: ')
         pprint(api_response)
    except ApiException as e:
         print ("Exception when calling ConceptresourceApi->get_concept_preferred_term_for_id_using_get: %s\n" % e)

    # create an instance of the API class
    api_instance = swagger_client.ConceptresourceApi(api_client)
    id = idNum # str | id
    language = 'en' # str | language
    version = 'version_example' # str | The version of the terminology product as date string

    try:
        # get the Fully Specified Name of concept with given id and language
        api_response = api_instance.get_concept_fully_specified_name_for_id_using_get(id, language, version)
        print('Fully specified name: ')
        fsn=api_response
        pprint(api_response)
        #print('--------------------')
    except ApiException as e:
        print ("Exception when calling ConceptresourceApi->get_concept_fully_specified_name_for_id_using_get: %s\n" % e)

    # create an instance of the API class
    api_instance_h = swagger_client.HierarchyresourceApi(api_client)

    id = idNum # str | id
    version = 'version_example' # str | The version of the terminology product as date string

    try:
        # get the children of concept with given id
        api_response_h = api_instance_h.get_children_for_id_using_get(id, version)
        print('Children of {}: '.format(fsn))
        print ("---------------------------")
        #pprint(api_response_h)
        #print('--------------------')
        #get fully specified names of children
        for childId in api_response_h:
            api_instance_c = swagger_client.ConceptresourceApi(api_client)
            capi_response = api_instance_c.get_concept_fully_specified_name_for_id_using_get(childId, language, version)
            print('Fully specified name of Child concept Id {} is: {}'.format(childId,capi_response))

    except ApiException as e:
        print("Exception when calling HierarchyresourceApi->get_children_for_id_using_get: %s\n" % e)

    # create an instance of the API class
    api_instance = swagger_client.HierarchyresourceApi(api_client)
    id = idNum # str | id
    version = 'version_example' # str | The version of the terminology product as date string

    try:
        # get an aggregate count of children, parent, ancestor and descendant counts for concept with given id
        api_response_count = api_instance.get_counts_for_id_using_get(id, version)
        print('Aggregate counts of {}: '.format(fsn))
        #pprint(api_response_count)
        #print('--------------------')
        for key,value in api_response_count.items():
            print('Count of {} is {}'.format(key,value))
    except ApiException as e:
        print ("Exception when calling HierarchyresourceApi->get_counts_for_id_using_get: %s\n") % e
    print('========================')

#get concept Id details for a list
conceptId=['707497007','445721008']
for cid in conceptId:
    getIdDetails(cid)
